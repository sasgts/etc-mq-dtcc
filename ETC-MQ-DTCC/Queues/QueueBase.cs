﻿using ETC_MQ_DTCC.Modules;
using IBM.WMQ;
using System;
using System.Configuration;

namespace ETC_MQ_DTCC.Queues
{
    public class QueueBase
    {
        private static readonly log4net.ILog log = LogHelper.GetLogger();
        private MQQueueManager queueManager;
        private MQQueue queueInput;
        private MQQueue queueOutput;
        private MQGetMessageOptions messageOptions;
        private MQPutMessageOptions putMessageOptions;
        protected string connectionString;
        private static string QUEUE_MANAGER_NAME;
        private static string QUEUE_NAME;

        public QueueBase(string queueManagerName, string queueName)
        {
            QUEUE_MANAGER_NAME = queueManagerName;
            QUEUE_NAME = queueName;

            connectionString = ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;

            queueManager = new MQQueueManager(queueManagerName);
            queueInput = queueManager.AccessQueue(queueName, MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
            queueOutput = queueManager.AccessQueue(queueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);

            messageOptions = new MQGetMessageOptions();
            messageOptions.Options |= MQC.MQGMO_WAIT | MQC.MQGMO_FAIL_IF_QUIESCING;
            // 3 seconds wait time or use MQC.MQEI_UNLIMITED
            messageOptions.WaitInterval = int.Parse(ConfigurationManager.AppSettings["MESSAGE_WAIT_INTERVAL"]);

            // creating a object for changing option while putting messages
            putMessageOptions = new MQPutMessageOptions();
            // adding the option for asynchronous put
            putMessageOptions.Options += MQC.MQPMO_ASYNC_RESPONSE;
        }

        public void GetMessage()
        {
            try
            {
                MQMessage mqMessage = new MQMessage();
                queueInput.Get(mqMessage, messageOptions);

                string message = mqMessage.ReadString(mqMessage.MessageLength);
                log.Info(message);
                log.Debug("Message received.");
                ParseMessage(message);
            }
            catch (MQException mqe)
            {
                if (mqe.ReasonCode == 2033)
                {
                    log.Debug("No message available.");
                }
                else
                {
                    throw mqe;
                }
            }
        }

        public void SendMessage(string messageString)
        {
            log.Warn(messageString);
            MQMessage message = new MQMessage();
            message.WriteString(messageString);
            queueOutput.Put(message);
            log.Debug("Message sent.");
        }

        public void SendMessageAsync(string messageString)
        {
            log.Warn(messageString);
            MQMessage message = new MQMessage();
            message.WriteString(messageString);
            queueOutput.Put(message, putMessageOptions);
            log.Debug("Message sent.");
        }

        public void CloseQueue()
        {
            try
            {
                if (queueInput != null)
                {
                    log.Debug("Closing " + QUEUE_NAME + " inbound queue.");
                    queueInput.Close();
                }
            }
            catch (MQException mqe)
            {
                log.Error("MQException caught: " + mqe.ReasonCode, mqe);
            }

            try
            {
                if (queueOutput != null)
                {
                    log.Debug("Closing " + QUEUE_NAME + " outboundqueue.");
                    queueOutput.Close();
                }
            }
            catch (MQException mqe)
            {
               log.Error("MQException caught: " + mqe.ReasonCode, mqe);
            }

            try
            {
                if (queueManager != null)
                {
                    log.Debug("Disconnecting " + QUEUE_MANAGER_NAME + " queue manager.");
                    queueManager.Disconnect();
                }
            }
            catch (MQException mqe)
            {
                log.Error("MQException caught: " + mqe.ReasonCode, mqe);
            }
        }

        protected virtual void ParseMessage(string message)
        {
        }
    }
}
