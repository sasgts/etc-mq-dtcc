﻿using Dapper;
using ETC_MQ_DTCC.Models;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using System.Xml.Serialization;

namespace ETC_MQ_DTCC.Queues
{
    internal partial class QueueCACO : QueueBase
    {
        private static readonly string queueManagerName = ConfigurationManager.AppSettings["QM__CACO"];
        private static readonly string queueName = ConfigurationManager.AppSettings["LQ__CACO"];

        public QueueCACO() : base(queueManagerName, queueName)
        {
        }

        protected override void ParseMessage(string message)
        {
            XmlDocument Doc = new XmlDocument();
            Doc.LoadXml(message);

            //Find the node from XML with name Document
            XmlNode xmlNode = Doc.GetElementsByTagName("Document").Item(0);

            XmlSerializer serial = new XmlSerializer(typeof(Document));
            Document data = (Document)serial.Deserialize(new XmlNodeReader(xmlNode));

            SaveMessage(data);
        }

        private void SaveMessage(Document data)
        {
            string eventType = data.CorpActnMvmntConf.CorpActnGnlInf.EvtTp.Item.ToString();
            string cusip = "";
            CorporateActionAmounts51 amtDtls = null;

            using (IDbConnection con = new SqlConnection(connectionString))
            {
                for (int i = 0; i < data.CorpActnMvmntConf.CorpActnGnlInf.FinInstrmId.OthrId.Length; i++)
                {
                    cusip = data.CorpActnMvmntConf.CorpActnGnlInf.FinInstrmId.OthrId[i].Id;


                    if (data.CorpActnMvmntConf.CorpActnConfDtls.CshMvmntDtls != null)
                    {
                        amtDtls = data.CorpActnMvmntConf.CorpActnConfDtls.CshMvmntDtls[i].AmtDtls;
                    }
                    else
                    {
                        amtDtls = null;
                    }

                    CACO newData = new CACO(eventType, cusip, amtDtls);
                    con.Execute("DTCC.CSHSET_MQ_SP", newData, commandType: CommandType.StoredProcedure);
                }
            }
        }
    }
}
