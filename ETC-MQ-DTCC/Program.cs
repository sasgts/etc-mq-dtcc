﻿using ETC_MQ_DTCC.Modules;
using ETC_MQ_DTCC.Queues;
using System;
using System.Configuration;
using System.IO;
using System.Threading;

//use xml config to configure log4net and watch for changes
[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace ETC_MQ_DTCC
{
    internal class Program
    {
        private static readonly log4net.ILog log = LogHelper.GetLogger();
        private static int CURRENT_ERROR_COUNT = 0;
        private static readonly int MAX_ERROR_COUNT = int.Parse(ConfigurationManager.AppSettings["MAX_ERROR_COUNT"]);
        private static QueueCACO Q_CACO = new QueueCACO();


        private static int Main(string[] args)
        {
            Thread caco = new Thread(RunCACO);
            caco.Start();

            Console.WriteLine("Press any key to close.");
            Console.ReadKey();

            caco.Abort();
            CloseQueues();
            return 0;
        }

        private static void CloseQueues()
        {
            Q_CACO.CloseQueue();
        }

        private static void ErrorLog(Exception e)
        {
            CURRENT_ERROR_COUNT++;
            log.Debug(e.Message);
            log.Error(e);

            if (CURRENT_ERROR_COUNT >= MAX_ERROR_COUNT)
            {
                CloseQueues();
                Environment.Exit(-1);
            }
        }

        private static void RunCACO()
        {
            //SEND TEST FILES
            try
            {
                string folder = ConfigurationManager.AppSettings["TEST_FILES_FOLDER"];
                string[] files = Directory.GetFiles(folder, "*.xml");
                string text = "";
                foreach (string file in files)
                {
                    text = File.ReadAllText(file);
                    Q_CACO.SendMessageAsync(text);
                }
            }
            catch (Exception e)
            {
                ErrorLog(e);
            }

            //LISTEN TO INCOMING MESSAGES
            while (CURRENT_ERROR_COUNT < MAX_ERROR_COUNT)
            {
                try
                {
                    Q_CACO.GetMessage();
                }
                catch (Exception e)
                {
                    ErrorLog(e);
                }
            }
        }
    }
}
