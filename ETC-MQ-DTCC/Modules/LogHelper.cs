﻿using System.Runtime.CompilerServices;

namespace ETC_MQ_DTCC.Modules
{
    public static class LogHelper
    {
        public static log4net.ILog GetLogger([CallerFilePath]string filename = "")
        {
            return log4net.LogManager.GetLogger(filename);
        }
    }
}
