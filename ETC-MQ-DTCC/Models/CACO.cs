﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETC_MQ_DTCC.Models
{
    /// <summary>
    /// CSHSET
    /// </summary>
    public class CACO
    {
        private string _EventType;
        private string _Cusip;
        private decimal _PstngAmt;
        private decimal _NetCshAmt;
        private decimal _CptlGn;
        private decimal _CshInLieuOfShr;
        private decimal _IntrstAmt;
        private decimal _AcrdIntrstAmt;

        public string Action { get; set; }
        public string EventType => _EventType;
        public string Cusip => _Cusip;
        public decimal? PstngAmt => _PstngAmt;
        public decimal? NetCshAmt => _NetCshAmt;
        public decimal? CptlGn => _CptlGn;
        public decimal? CshInLieuOfShr => _CshInLieuOfShr;
        public decimal? IntrstAmt => _IntrstAmt;
        public decimal? AcrdIntrstAmt => _AcrdIntrstAmt;

        public CACO(string eventType, string cusip, CorporateActionAmounts51 amtDtls)
        {
            Action = "ADD";

            _EventType = eventType;
            _Cusip = cusip;
            if (amtDtls != null)
            {
                if (amtDtls.PstngAmt != null)
                {
                    _PstngAmt = amtDtls.PstngAmt.Value;
                }
                if (amtDtls.NetCshAmt != null)
                {
                    _NetCshAmt = amtDtls.NetCshAmt.Value;
                }
                if (amtDtls.CptlGn != null)
                {
                    _CptlGn = amtDtls.CptlGn.Value;
                }
                if (amtDtls.CshInLieuOfShr != null)
                {
                    _CshInLieuOfShr = amtDtls.CshInLieuOfShr.Value;
                }
                if (amtDtls.IntrstAmt != null)
                {
                    _IntrstAmt = amtDtls.IntrstAmt.Value;
                }
                if (amtDtls.AcrdIntrstAmt != null)
                {
                    _AcrdIntrstAmt = amtDtls.AcrdIntrstAmt.Value;
                }
            }
        }
    }
}
