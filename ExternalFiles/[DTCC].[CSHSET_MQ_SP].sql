USE [Matrix]
GO
/****** Object:  StoredProcedure [DTCC].[CSHSET_MQ_SP]    Script Date: 26/11/2019 10:56:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [DTCC].[CSHSET_MQ_SP]
	@Action varchar(30) = 'VIEW'
,	@EventType varchar(4) = ''
,	@Cusip varchar(31) = ''
,	@PstngAmt decimal(18,6) = 0
,	@NetCshAmt decimal(18,6) = 0
,	@CptlGn decimal(18,6) = 0
,	@CshInLieuOfShr decimal(18,6) = 0
,	@IntrstAmt decimal(18,6) = 0
,	@AcrdIntrstAmt decimal(18,6) = 0
AS


DECLARE @SystemDate DATE = MTX.SystemDate()

IF @SystemDate IS NULL
	SET @SystemDate = GETDATE()
----------------------------------------------------
-- ADD: 
----------------------------------------------------	
IF @Action = 'ADD'
BEGIN
	INSERT INTO DTCC.CSHSET_MQ([System Date], EventType,Cusip,PstngAmt,NetCshAmt,CptlGn,CshInLieuOfShr,IntrstAmt,AcrdIntrstAmt)
	VALUES(@SystemDate,@EventType,@Cusip,@PstngAmt,@NetCshAmt,@CptlGn,@CshInLieuOfShr,@IntrstAmt,@AcrdIntrstAmt)
END

