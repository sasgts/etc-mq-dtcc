USE [Matrix]
GO

/****** Object:  Table [DTCC].[CSHSET_MQ]    Script Date: 26/11/2019 10:55:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [DTCC].[CSHSET_MQ](
	[System Date] [date] NOT NULL,
	[EventType] [varchar](4) NULL,
	[Cusip] [varchar](31) NULL,
	[PstngAmt] [decimal](18, 6) NULL,
	[NetCshAmt] [decimal](18, 6) NULL,
	[CptlGn] [decimal](18, 6) NULL,
	[CshInLieuOfShr] [decimal](18, 6) NULL,
	[IntrstAmt] [decimal](18, 6) NULL,
	[AcrdIntrstAmt] [decimal](18, 6) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


